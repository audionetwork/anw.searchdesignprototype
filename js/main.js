jQuery(document).ready(function($) {

  // var widthOfHidden = function() {
  //   return (($('.search-filters__container').outerWidth()) - widthOfList());
  // };

  // var widthOfList = function() {
  //   var itemsWidth = 0;
  //   $('.search-filters__mask').find('.facet').each(function() {
  //     var itemWidth = $(this).outerWidth();
  //     itemsWidth += itemWidth;
  //   });
  //   return itemsWidth;
  // };

  // var navRight = $('.search-filters__nav-right');
  // var navLeft = $('.search-filters__nav-left');

  // navRight.click(function() {
  //   navLeft.fadeIn('fast');
  //   $(this).fadeOut('fast');

  //   $('ol').data('class', 'search-filters__mask').animate({
  //     left: "+=" + widthOfHidden() + "px"
  //   }, 'fast', function() {});
  // });

  // navLeft.click(function() {
  //   navRight.fadeIn('fast');
  //   $(this).fadeOut('fast');

  //   $('ol').data('class', 'search-filters__mask').animate({
  //     left: 0
  //   }, 'fast', function() {});
  // });


  // Dropdown toggle and click away
  $('html').click(function(e) {
    var $target = $(e.target);

    if ($target.is('.facet__toggle')) {
      $target.parents('.facet').toggleClass('active');
      $('.facet').not($target.parents('.facet')).removeClass('active');
    } else if ($target.is('.facet__dropdown-item')) {
      $target.toggleClass('selected');

      let $value = $target.text();

      let $cat = $('<span/>', {
        'class': 'search-terms__term',
        'id': $value
      }).text($value);

      if (!$target.hasClass('selected')) {
        $('.search-terms__term').filter(function() {
          return $(this).attr('id') === $value;
        }).remove();
      } else {
        $('.search-terms').append($cat);
      }

      if ($target.parent().children('.facet__dropdown-item.selected').length !== 0) {
        $target.parents('.facet').addClass('selected');
        //$('.search-terms').append($cat);
      } else {
        $target.parents('.facet').removeClass('selected');
        //$('.search-terms__term').data('cat', $target.text()).remove();
      }
    } else if ($target.is('.facet__dropdown-btn')) {

      $target.parent().siblings('.facet__dropdown').each(function(){
          $(this).find('.facet__dropdown-item--secondary').toggleClass('hidden');
      });

      $target.text(function(i, text){
          return text === "SHOW LESS FILTERS" ? "SHOW ALL FILTERS" : "SHOW LESS FILTERS";
      })

    }

    else {
      $('.facet').removeClass('active');
    }
  });


  // Scroll facets
  $('.facet__dropdown').bind('mousewheel DOMMouseScroll', function(e) {
    var scrollTo = null;

    if (e.type == 'mousewheel') {
      scrollTo = (e.originalEvent.wheelDelta * -1);
    } else if (e.type == 'DOMMouseScroll') {
      scrollTo = 40 * e.originalEvent.detail;
    }

    if (scrollTo) {
      e.preventDefault();
      $(this).scrollTop(scrollTo + $(this).scrollTop());
    }
  });


});
